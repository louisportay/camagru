-- name: find-user-by-name-or-mail
SELECT username FROM users WHERE username=$1 OR email=$2;

-- name: create-user
INSERT INTO users (username, email, passd, confirmation_token, confirmation_token_sent_at) VALUES ($1, $2, $3, $4, now());

-- name: get-passd-by-email
SELECT passd FROM users WHERE email=$1;
