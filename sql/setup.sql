-- name: create-users-table
CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "username" varchar(64) UNIQUE NOT NULL,
  "email" varchar(64) UNIQUE NOT NULL,
  "passd" varchar(64) NOT NULL,
  "created_at" timestamp DEFAULT (now()),
  "mail_notif" bool DEFAULT true,
  "confirmed" bool DEFAULT false,
  "confirmation_token" varchar(64),
  "confirmation_token_sent_at" timestamp,
  "reset_token" varchar(64),
  "reset_token_sent_at" timestamp
);

-- name: create-pictures-table
CREATE TABLE "pictures" (
  "id" SERIAL PRIMARY KEY,
  "filepath" varchar(256) UNIQUE NOT NULL,
  "created_at" timestamp DEFAULT (now()),
  "user_id" int NOT NULL
);

-- name: create-likes-table
CREATE TABLE "likes" (
  "id" SERIAL PRIMARY KEY,
  "created_at" timestamp DEFAULT (now()),
  "user_id" int NOT NULL,
  "picture_id" int NOT NULL
);

-- name: create-comments-table
CREATE TABLE "comments" (
  "id" SERIAL PRIMARY KEY,
  "content" varchar(1024),
  "created_at" timestamp DEFAULT (now()),
  "user_id" int NOT NULL,
  "picture_id" int NOT NULL
);

-- name: add-foreign-keys
ALTER TABLE "pictures" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
ALTER TABLE "likes" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
ALTER TABLE "likes" ADD FOREIGN KEY ("picture_id") REFERENCES "pictures" ("id");
ALTER TABLE "comments" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
ALTER TABLE "comments" ADD FOREIGN KEY ("picture_id") REFERENCES "pictures" ("id");


-- name: schema-reset
DROP TABLE users, comments, likes, pictures CASCADE;
