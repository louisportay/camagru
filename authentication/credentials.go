package authentication

import (
	"gitlab.com/louisportay/camagru/dbase"
	"net/http"
	"encoding/json"
	"errors"
	"regexp"
)

type Credentials struct {
	Username string `json:"username"`
	Email string `json:"email"`
	Password string `json:"password"`
}

var (
	mail_re = regexp.MustCompile("^[a-zA-Z0-9]+@[a-z]+.(com|fr|net)$")
	username_re = regexp.MustCompile("^[a-zA-Z0-9]{1,16}$")
	PassTooShortErr = errors.New("Password is too short")
	PassNotEqErr = errors.New("Passwords do not match")
	InvalidLoginLengthErr = errors.New("Username length is not valid")
	InvalidEmailFormatErr = errors.New("Mail is not valid")
	InvalidUsernameErr = errors.New("Username is not valid")
	UsernameTaken = errors.New("This username is already in use")
	MailTaken = errors.New("This email adress is already in use")
)

func RetrieveCreds(r *http.Request) (c Credentials, e error) {
	v, ok := r.Header["Content-Type"]
	if ok && v[0] == "application/x-www-form-urlencoded" {
		e = r.ParseForm()
		c.Email, c.Username, c.Password = r.Form.Get("email"), r.Form.Get("username"), r.Form.Get("password")
	} else if ok && v[0] == "application/json" {
		e = json.NewDecoder(r.Body).Decode(&c)
	} else {
		e = errors.New("Unknown Content-Type")
	}
	return
}

func (c Credentials) Valid(passdRepeat string) (error) {
	for _, condition := range []struct{f func() bool; err error} {
		{ func()(bool){ return len(c.Password) >= 12 }, PassTooShortErr},
		{ func()(bool){ return c.Password == passdRepeat}, PassNotEqErr},
		{ func()(bool){ return len(c.Username) > 0 && len(c.Username) <= 32}, InvalidLoginLengthErr},
		{ func()(bool){ return mail_re.MatchString(c.Email)}, InvalidEmailFormatErr},
		{ func()(bool){ return username_re.MatchString(c.Username)}, InvalidUsernameErr},

	}{
		if !condition.f() {
			return condition.err
		}
	}
	return nil
}

func (c Credentials) AlreadyUsed() (error, int)  {
	rows, err := dbase.Query("find-user-by-name-or-mail", c.Username, c.Email)
	if err != nil {
		return dbase.Unreachable, http.StatusInternalServerError
	}
	defer rows.Close()
	if rows.Next() == true {
		var username string
		if rows.Scan(&username) != nil {
			return dbase.Unreachable, http.StatusInternalServerError
		}
		if username == c.Username {
			return UsernameTaken, http.StatusBadRequest
		} else {
			return MailTaken, http.StatusBadRequest
		}
	} else {
		return nil, 0
	}
}
