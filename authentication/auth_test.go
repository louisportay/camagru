package authentication

import (
	"testing"
	"fmt"
)

func TestGenToken (t *testing.T) {
		testname := fmt.Sprintf("Token Generation")
		t.Run(testname, func(t *testing.T) {
			r, e := GenToken()
			if e != nil {
				t.Errorf("GenToken got %v", e)
			} else if len(r) != 32  {
				t.Errorf("token length is %d, want %d", len(r), 32)
			}
		})
}
