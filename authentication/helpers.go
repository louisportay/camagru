package authentication

import (
	"crypto/rand"
	"encoding/hex"
)


func GenToken() (string, error) {
	b := make([]byte, 16)
	if _, err := rand.Read(b); err != nil {
		return "", err
	} else {
		return hex.EncodeToString(b), nil
	}
}
