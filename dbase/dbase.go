package dbase

import (
	_ "github.com/joho/godotenv/autoload"
	"database/sql"
	"log"
	"fmt"
	"os"
	"github.com/gchaincl/dotsql"
	"github.com/gomodule/redigo/redis"
	"errors"
	_ "github.com/lib/pq"
)

var (
	db = Open(os.Getenv("DB_USER"), os.Getenv("DB_PASSD"), os.Getenv("DB_NAME"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT"))
	dot = loadQueries(os.Getenv("SQL_QUERY")/*, os.Getenv("SQL_SETUP")*/)
	Cache = initCache(os.Getenv("REDIS_URL"))
)

var (
	Unreachable = errors.New("Database is unreachable")
)

func Open(user, passd, name, host, port string) *sql.DB {
	conn := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s", user, passd, name, host, port)
	db, err := sql.Open("postgres", conn)
	if err != nil {
		log.Fatal(err)
	}
	err = db.Ping()
	if  err != nil {
		log.Fatal(err)
	}
	return db
}

func Close() {
	db.Close()
}

func initCache(rawurl string) redis.Conn {
	cache, err := redis.DialURL(rawurl)
	if err != nil {
		log.Fatal(err)
	}
	return cache
}

func loadQueries(filepaths ...string) (sqlqueries *dotsql.DotSql) {
	for i, path := range filepaths {
		d, err := dotsql.LoadFromFile(path)
		if i == 0 {
			sqlqueries = d
		} else {
			sqlqueries = dotsql.Merge(sqlqueries, d)
		}
		if err != nil {
			log.Fatal(err)
		}
	}
	return sqlqueries
}

func Query(query string, args ...interface{}) (*sql.Rows, error) {
	return dot.Query(db, query, args...)
}

func Exec(exec string, args ...interface{}) (sql.Result, error) {
	return dot.Exec(db, exec, args...)
}
