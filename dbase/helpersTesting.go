package dbase

import (
	"database/sql"
	"log"
)

func setupPsql(db *sql.DB) {
	sqlcmds := []string{"create-users-table", "create-pictures-table", "create-comments-table", "create-likes-table", "add-foreign-keys"}

	for _, cmd := range sqlcmds {
		_, err := dot.Exec(db, cmd)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func nukePsql(db *sql.DB, sqlSetupFile string) {
	_, err := dot.Exec(db, "schema-reset")
	if err != nil {
		log.Fatal(err)
	}
}
