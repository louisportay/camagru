package main

import (
	"testing"
	"fmt"
)

func TestretrieveCreds (t *testing.T) {
	var tests = http.Request {

		want int
	}{
		{1, 2, 3},
	}
	for _, tt := range tests {
		testname := fmt.Sprintf("%d+%d", tt.a, tt.b)
		t.Run(testname, func(t *testing.T) {
			r := SomeFunc(tt.a, tt.b)
			if r != tt.want {
				t.Errorf("got %d, want %d", r, tt.want)
			}
		})
	}
}

