module gitlab.com/louisportay/camagru

go 1.14

require (
	github.com/gchaincl/dotsql v1.0.0
	github.com/gomodule/redigo v1.8.2
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.3.0
	github.com/satori/go.uuid v1.2.0
	goji.io v2.0.2+incompatible
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9
)
