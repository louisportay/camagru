package main

import (
	"gitlab.com/louisportay/camagru/dbase"
	"net/http"
	"html/template"
	"log"
	//"fmt"
	"goji.io"
	"goji.io/pat"
)

var (
	templates = template.Must(template.ParseFiles("templates/signin.tmpl", "templates/signup.tmpl"))
)

func registerRoutes(mux *goji.Mux) {

	// GET

	mux.HandleFunc(pat.Get("/"), home) // Gallery
	mux.HandleFunc(pat.Get("/signup"), getSignup)
	mux.HandleFunc(pat.Get("/signin"), getSignin)
	mux.HandleFunc(pat.Get("/me"), profile)

	//http.HandleFunc("/:picture_id", picture)
	//http.HandleFunc("/photomaton", photomaton)

	// POST

	mux.HandleFunc(pat.Post("/signup"), postSignup)
	mux.HandleFunc(pat.Post("/signin"), postSignin)
}

func main() {
	defer dbase.Close()
	mux := goji.NewMux()
	registerRoutes(mux)
	//dbase.SetupPsql(db, "sql/setup.sql")
	//dbase.NukePsql(db, "sql/setup.sql")
	log.Fatal(http.ListenAndServe(":8000", mux))

	/*username := "lportay"
	rows, err := db.Query("SELECT passd FROM users WHERE username = $1", username)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var passd string
		if err := rows.Scan(&passd); err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%s passd is %s\n", username, passd)
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}*/
}
