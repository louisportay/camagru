package main

import (
//	"fmt"
	"gitlab.com/louisportay/camagru/authentication"
	"gitlab.com/louisportay/camagru/dbase"
	"net/http"
	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"time"
)

func home(w http.ResponseWriter, r *http.Request) {

}

func getSignup(w http.ResponseWriter, r *http.Request) {
	err := templates.ExecuteTemplate(w, "signup.tmpl", nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func getSignin(w http.ResponseWriter, r *http.Request) {
	err := templates.ExecuteTemplate(w, "signin.tmpl", nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func postSignup(w http.ResponseWriter, r *http.Request) {
	creds, err := authentication.RetrieveCreds(r)
	if err != nil {
		http.Error(w, "Credentials could not be retrieved", http.StatusBadRequest)
		return
	}
	err = creds.Valid(r.Form.Get("password-rpt"))
	if err != nil {
		http.Error(w, "Passwords were different", http.StatusBadRequest)
		return
	}
	err, code := creds.AlreadyUsed()
	if err != nil {
		http.Error(w, err.Error(), code)
		return
	}
	hashPassd, errP := bcrypt.GenerateFromPassword([]byte(r.Form.Get("password")), 10)
	token, errT := authentication.GenToken()
	if errP != nil || errT != nil {
		http.Error(w, "Failed to save user in database", http.StatusInternalServerError)
		return
	}
	_, err = dbase.Exec("create-user", r.Form.Get("username"), r.Form.Get("email"), hashPassd, token)
	if err != nil {
		http.Error(w, "Failed to save user in database", http.StatusInternalServerError)
		return
	}

	// send mail

	w.WriteHeader(http.StatusCreated)
}

func postSignin(w http.ResponseWriter, r *http.Request) {
	creds, err := authentication.RetrieveCreds(r)
	if err != nil {
		http.Error(w, "Credentials could not be retrieved", http.StatusBadRequest)
		return
	}

	rows, e := dbase.Query("get-passd-by-email", creds.Email)
	if e != nil {
		http.Error(w, "Could not query database", http.StatusInternalServerError)
		return
	}
	defer rows.Close()
	if rows.Next() == false {
		http.Error(w, "No user for this mail", http.StatusUnauthorized)
		return
	}
	var hash []byte
	rows.Scan(&hash)
	e = bcrypt.CompareHashAndPassword(hash, []byte(creds.Password))
	if e != nil {
		http.Error(w, "Incorrect Password", http.StatusUnauthorized)
		return
	}

	sessionToken := uuid.NewV4().String()
	_, e = dbase.Cache.Do("SETEX", sessionToken, "120", creds.Username)
	if e != nil {
		http.Error(w, "Could not sign you in", http.StatusInternalServerError)
		return
	}
	http.SetCookie(w, &http.Cookie{
		Name: "session_token",
		Value: sessionToken,
		Expires: time.Now().Add(120 * time.Second),
	})
}


func profile(w http.ResponseWriter, r *http.Request) {

}
